Welcome to the ~"CI/CD" Team issue tracker.

This issue tracker is a place where you can discuss any issues related to the
CI/CD Team or its work that do not belong in a specific project maintained by
CI/CD.

# Quick links

[CI/CD handbook](https://about.gitlab.com/handbook/engineering/ops-backend/ci-cd/)
